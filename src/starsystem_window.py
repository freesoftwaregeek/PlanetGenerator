# Author: LuminousLizard
# Licence: EUPL 1.2

import dearpygui.dearpygui as dpg


def callback_closeWindow(sender):
    dpg.delete_item(sender)


def callback_openStarSystemGeneratorWindow(sender, callback=callback_closeWindow):
    if dpg.does_item_exist("StarSystemGeneratorWindow"):
        print("Window already exists")
    else:
        with dpg.window(label="Star system generator",
                        tag="StarSystemGeneratorWindow",
                        pos=(200, 200),
                        width=300,
                        height=300):
            pass
