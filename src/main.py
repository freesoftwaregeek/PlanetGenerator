# Author: LuminousLizard
# Licence: EUPL 1.2

import dearpygui.dearpygui as dpg
import dearpygui.demo as demo
import planet_window
import starsystem_window


def callback_closeWindow(sender):
    dpg.delete_item(sender)


def callback_openFile(sender, app_data):
    xmlFile = open(app_data["file_path_name"], "r")
    xmlFile_data = xmlFile.read()
    xmlFile.close()
    dpg.set_value("XMLText", xmlFile_data)


def callback_saveFile(sender, app_data):
    xmlFile_data = dpg.get_value("XMLText")
    xmlFile = open(app_data["file_path_name"], "w")
    xmlFile.write(xmlFile_data)
    xmlFile.close()


def callback_cancelFile(sender, app_data):
    pass


def callback_openAboutWindow(sender, app_data):
    pass


def callback_openXMLEditor(sender, callback_closeWindow):
    if dpg.does_item_exist("XMLEditorWindow"):
        print("Window already exists")
    else:
        with dpg.window(label="XML-Editor",
                        tag="XMLEditorWindow",
                        pos=(100, 50),
                        width=600,
                        height=700,
                        no_resize=True):
            with dpg.group(horizontal=True):
                dpg.add_button(label="Open XML file",
                               callback=lambda: dpg.show_item("file_dialog_open"))
                dpg.add_button(label="Save XML file",
                               callback=lambda: dpg.show_item("file_dialog_save"))
            dpg.add_input_text(tag="XMLText",
                               multiline=True,
                               width=580,
                               height=640)

# Create context, viewport and setup dearpygui
dpg.create_context()
dpg.create_viewport(title="Planet generator", width=1024, height=768)
dpg.setup_dearpygui()

# Menu bar
with dpg.viewport_menu_bar():
    dpg.add_menu_item(label="Planet generator",
                      callback=planet_window.callback_openPlanetGeneratorWindow)
    dpg.add_menu_item(label="Star system generator",
                      callback=starsystem_window.callback_openStarSystemGeneratorWindow)
    dpg.add_menu_item(label="Open XML editor",
                      callback=callback_openXMLEditor)
    dpg.add_menu_item(label="About",
                      callback=callback_openAboutWindow)

with dpg.file_dialog(directory_selector=False,
                     show=False,
                     callback=callback_openFile,
                     tag="file_dialog_open",
                     cancel_callback=callback_cancelFile,
                     width=700,
                     height=300):
    dpg.add_file_extension(".xml", color=(0, 0, 255, 255))

with dpg.file_dialog(directory_selector=False,
                     show=False,
                     callback=callback_saveFile,
                     tag="file_dialog_save",
                     cancel_callback=callback_cancelFile,
                     width=700,
                     height=300):
    dpg.add_file_extension(".xml", color=(0, 0, 255, 255))

dpg.show_viewport()
dpg.start_dearpygui()
dpg.destroy_context()
