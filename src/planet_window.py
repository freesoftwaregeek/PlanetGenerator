# Author: LuminousLizard
# Licence: EUPL 1.2

import dearpygui.dearpygui as dpg
import planet_generator


def callback_closeWindow(sender):
    dpg.delete_item(sender)


def callback_openPlanetGeneratorWindow(sender, callback=callback_closeWindow):
    if dpg.does_item_exist("PlanetGeneratorWindow"):
        print("Window already exists")
    else:
        with dpg.window(label="Planet generator",
                        tag="PlanetGeneratorWindow",
                        pos=(200, 200),
                        width=500,
                        height=300):
            dpg.add_button(label="Generate planet", callback=callback_startGenerator)
            dpg.add_text(tag="PlanetOutputText")


def callback_startGenerator():
    Planet1 = planet_generator.Planet()
    planet_data = Planet1.generate_planet(debug=False)

    text = ""
    for key, value_list in planet_data.items():
        values = ""
        if type(value_list) is list:
            for value in value_list:
                values = values + value + " "
        else:
            values = str(value_list)
        text = text + key + ": " + values + "\n"

    dpg.set_value("PlanetOutputText", text)
